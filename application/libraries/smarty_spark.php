<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once(APPPATH .'libraries/smarty/Smarty.class.php');
 
class Smarty_Spark extends Smarty {
 
    function __construct()
    {
        date_default_timezone_set('America/Phoenix');
 
        parent::__construct();
        $this->setTemplateDir(APPPATH . 'views');
        $this->setCompileDir(APPPATH . 'views/compiled');
        $this->setConfigDir(APPPATH . 'libraries/smarty/configs');
        $this->setCacheDir(APPPATH . 'libraries/smarty/cache');
 
        $this->assign( 'APPPATH', APPPATH );
        $this->assign( 'BASEPATH', BASEPATH );
        // $this->caching = Smarty::CACHING_LIFETIME_CURRENT; // Does something <img src="http://searchdaily.net/wp-includes/images/smilies/icon_smile.gif" alt="icon smile CodeIgniter 2 Smarty 3 integration" class="wp-smiley" title="CodeIgniter 2 Smarty 3 integration"> 
        if ( method_exists( $this, 'assignByRef') )
        {
            $ci =& get_instance();
            $this->assignByRef("ci", $ci);
        }
        $this->force_compile = 1;
        $this->caching = true;
        $this->cache_lifetime = 120;
 
        //log_message('debug', "Smarty Class Initialized");
    }

    function parse($template_name, $data = array(), $output = TRUE)
    {
        if (strpos($template_name, '.') === FALSE && strpos($template_name, ':') === FALSE) {
            $template_name .= '.tpl';
        }

        foreach( $data as $dkey => $dval )
        {
            $this->assign($dkey, $dval);
        }

        if( $output )
        {
            parent::display($template_name);
        }
        else
        {
            return parent::fetch($template_name);
        }
    }
}
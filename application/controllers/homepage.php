<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {
	public function index()
	{
        $data = array(
            'name' => 'John Doe',
        );
        $res = $this->smarty_spark->parse('index', $data, TRUE);
	}
}

/* End of file homepage.php */
/* Location: ./application/controllers/homepage.php */